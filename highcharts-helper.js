/*
|--------------------------------------------------------------------------
| highcharts-helper.js
|--------------------------------------------------------------------------
| 
| The Highchart object expects two sources of data: series and categories. 
| This module helps to format both kinds through the 'formatters' const variable, 
| that maps the chart type with the desired function. This way, the client can 
| call formatSeries() and formatCategories() by passing a set of data and a 
| type, allowing dynamic charts, as well decopling between the chart 
| itself and the information being displayed.
| 
*/

/**
 * Maps series and categories with theirs respective format functions.
 * @type {Object}
 */
const formatters = {
    series: {
        deliveries: formatSeriesForDeliveries,
        opens: formatSeriesForOpen,
        clicks: formatSeriesForClick,
        conversions: formatSeriesForConversion,
    },

    categories: {
        deliveries: formatCategoriesForDeliveries,
        opens: formatCategoriesForOpen,
        clicks: formatCategoriesForClick,
        conversions: formatCategoriesForConversion,
    },
}

/**
 * Handle series formatting given a data array and the type of the chart.
 * 
 * @param  {Array}      data  Contains the data to be formatted
 * @param  {string}     type  Type of the chart. Check the 'formatters' const variable
 * @return {mixed|null}       Formatted series or null
 */
export function formatSeries(data, type) {
    if (formatters.series[type] == undefined) {
        return;
    }

    return formatters.series[type](data);
}

/**
 * Handle categories formatting given a data array and the type of the event chart.
 * 
 * @param  {Array}      data    Contains the data to be formatted
 * @param  {string}     event  Type of the event chart. Check the 'formatters' const variable
 * @return {mixed|null}         Formatted categories or null
 */
export function formatCategories(data, event) {
    if (formatters.categories[event] == undefined) {
        return;
    }

    return formatters.categories[event](data);
}

/**
 * Default series format.
 * 
 * @param  {Array}  data        [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @param  {string} date_field  Field used to align the action with the scale (X axis)
 * @return {Array}              [[name: '...', data[null, 12, 25, null, ...], ...]]
 */
function formatSeriesDefault(data, date_field) {
    // All available dates - used to organize data.
    var dates = Array.from(new Set(data.map(item => item[date_field])));
    
    // Series formated.
    var result = [];

    // Preparing null slots to indicate that no data is present for that period.
    // By default, the number of slots is the total of periods (X axis scale).
    var slots = getSeriesDataSlots(dates)

    data.map(serie => {
        // Name of the serie must be unique.
        var name = formatSerieName(serie);

        // Use the name to check if we already added the 
        // given serie to the 'results' array. If positive, 
        // get the index and update that element.
        var index = result.findIndex(item => item.name == name)

        // If no index is found, we must create the serie and add to 'results' array.
        if (index == -1) {
            // Need to manually created a new object to represent a serie,
            // because Javascript pushes objects by reference. The 'data' 
            // property has the same number of 'null' values as the 
            // length of the dates (scale), so we can indicate in
            // which period there wasn't any data.
            var obj = JSON.parse(JSON.stringify({name: name, data: slots}));

            // Keep track of the result index, so we can check 
            // if there is data for all available dates.
            index = result.push(obj) - 1;
        }

        // Compare each date with the current element date (serie.dt) and add to 
        // the 'data' property. Since 'data' already has null values, 
        // the resulting object would be something like this:
        // - For a total of 3 dates in total: ['08/12', '08/13', '08/14']
        // - Initial serie.data is [null, null, null]
        // - If the serie looks like [{dt: '08/13', ct: 12}]
        // - Results in [null, 12, null]
        dates.map((date, i) => {
            if (serie[date_field] == date) {
                result[index].data.splice(i, 1, serie.ct)
            }
        });
    })

    result = changeNullToZeroAfterFirstNonNullValue(result);

    return result;
}

/**
 * There should not be any null values after inserting the first serie.ct in 
 * the result array, otherwise, we may end up with a 'broken' chart line. 
 * To solve this issue, we make sure that the given serie has at least 
 * one data entry. After that, change all null values to 0 for that 
 * object starting from the first non-null entry.
 * 
 * Example: Given the array [null, null, 2, 3, null, 7, null]
 * Returns: [null, null, 2, 3, 0, 7, 0]
 * 
 * @param  {array} data Data to be formatted
 * @return {array}      Array with null values changed to 0 after first non null index, if any
 */
function changeNullToZeroAfterFirstNonNullValue(data) {
    data.map((item) => {
        let firstNonNullIndex = null;
        let length = item.data.length;
        
        // Find the first non null index, if any.
        for (let i = 0; i <= length; i++) {
            if (item.data[i] != null) {
                firstNonNullIndex = i;
                break;
            }
        }

        // Change each null value to 0 after the first null index, 
        // except the last null value, that must be null 
        // to indicate the end of the array.
        if (firstNonNullIndex != null) {
            var lastIndex = item.data.length;

            for (let i = firstNonNullIndex; i <= length; i++) {
                if (item.data[i] == null && i != lastIndex) {
                    item.data[i] = 0;
                }
            }
        }
    });

    return data;
}

/**
 * Format series for the Delivery Event type.
 * 
 * @param  {Array} data  Data to be formatted
 * @return {Array}       [[name: '...', data[null, 12, 25, null, ...], ...]]
 */
function formatSeriesForDeliveries(data) {

    return formatSeriesDefault(data, 'dt')
}

/**
 * Format series for the Open Event type.
 * 
 * @param  {Array} data  Data to be formatted
 * @return {Array}       [[name: '...', data[null, 12, 25, null, ...], ...]]
 */
function formatSeriesForOpen(data) {
    return formatSeriesDefault(data, 'dt')
}

/**
 * Format series for the Click Event type.
 * 
 * @param  {Array} data  Data to be formatted
 * @return {Array}       [[name: '...', data[null, 12, 25, null, ...], ...]]
 */
function formatSeriesForClick(data) {
    return formatSeriesDefault(data, 'dt')
}

/**
 * Format series for the Conversion Event type.
 * 
 * @param  {Array} data  Data to be formatted
 * @return {Array}       [[name: '...', data[null, 12, 25, null, ...], ...]]
 */
function formatSeriesForConversion(data) {
    return formatSeriesDefault(data, 'dt')
}

/**
 * Default function for getting all categories (X axis scale).
 * By default, retrieve unique dates, given a 'field' name.
 * 
 * @param  {Array} data [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @return {Array}      ['08/12', '08/13', ...]
 */
function formatCategoriesByDateDefault(data, field) {
    // Format all date categories.
    let categories = data.map(item => formatDateCategory(item[field]));

    // Return unique items.
    return Array.from(new Set(categories))
}

/**
 * Format categories for the Deliveries Chart.
 * 
 * @param  {Array} data [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @return {Array}      ['08/12', '08/13', ...]
 */
function formatCategoriesForDeliveries(data) {
    return formatCategoriesByDateDefault(data, 'dt');
}

/**
 * Format categories for the Open Chart.
 * 
 * @param  {Array} data [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @return {Array}      ['08/12', '08/13', ...]
 */
function formatCategoriesForOpen(data) {
    return formatCategoriesByDateDefault(data, 'dt');
}

/**
 * Format categories for the Click Chart.
 * 
 * @param  {Array} data [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @return {Array}      ['08/12', '08/13', ...]
 */
function formatCategoriesForClick(data) {
    return formatCategoriesByDateDefault(data, 'dt');
}

/**
 * Format categories for the Conversion Chart.
 * 
 * @param  {Array} data [{dt: string, series_id: integer, offset_days: integer, ct: integer}, ...]
 * @return {Array}      ['08/12', '08/13', ...]
 */
function formatCategoriesForConversion(data) {
    return formatCategoriesByDateDefault(data, 'dt');
}

/**
 * Get slots for using in the 'data' property of the series.
 * 
 * @param  {Array} dates  X axis scale value
 * @return {Array}        Array containing null values
 */
function getSeriesDataSlots(dates) {
    let slots = [];
    
    dates.map(date => slots.push(null));

    return slots;
}

/**
 * Format the serie name to be displayed.
 * 
 * @param  {Object} serie  Expects 'offset_days' and 'series_id' properties
 * @return {string}        Formated string
 */
function formatSerieName(serie) {
    const days = serie.offset_days;
    const id = serie.series_id;

    if (days != undefined || id != undefined) {
        return days + " Days (Series ID: " + id + ")";
    }

    return 'Total';
}

/**
 * Format dates in string to be displayed in the chart.
 * 
 * @param  {string} date String in the format '2017-08-13'
 * @return {string}      For input '2017-08-13', returns '08/13'
 */
function formatDateCategory(date) {
    return new Date(date).toISOString().slice(5, 10).replace("-", "/")
}