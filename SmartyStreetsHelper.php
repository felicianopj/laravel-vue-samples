<?php

namespace App\Helpers;

use SmartyStreets;
use App\Api\V1\Models\User;

/*
|--------------------------------------------------------------------------
| SmartyStreetsHelper.php
|--------------------------------------------------------------------------
| 
| This class helps in address validation. Useful for verifying if the 
| provided address exists or it is duplicated, as well format 
| correctly before saving on the database.
| 
*/

class SmartyStreetsHelper
{
    /**
     * Invalid address status description.
     * @var string
     */
    protected static $INVALID_STATUS    = 'invalid';

    /**
     * Duplicated address status description.
     * @var string
     */
    protected static $DUPLICATED_STATUS = 'duplicated';

    /**
     * Return address information provided by the SmartyStreets API.
     * All parameters can be empty because SmartyStreets will 
     * provide a way to retrieve address using only the $state
     * and $zip, for example, or just with the $street name.
     * 
     * @param  string|empty $street
     * @param  string|empty $state
     * @param  string|empty $city
     * @param  string|empty $zip
     * @return array        Address information or empty, if invalid
     */
    public static function getAddressInfo($street = '', $state = '', $city = '', $zip = '')
    {
        $address = [
            'street' => $street,
            'city'   => $city,
            'state'  => $state,
            'zip'    => $zip,
        ];

        $address = SmartyStreets::addressQuickVerify($address);

        if (empty($address)) {
            return [];
        }

        return [
            'street'    => $address['components']['primary_number'] . ' ' . $address['components']['street_name'],
            'city'      => $address['components']['city_name'],
            'state'     => $address['components']['state_abbreviation'],
            'zip'       => $address['components']['zipcode'],
            'latitude'  => $address['metadata']['latitude'],
            'longitude' => $address['metadata']['longitude'],
        ];
    }

    /**
     * Get one address for the given user information.
     * 
     * @param  array $user_info
     * @return mixed
     */
    public static function getOneAddress(array $user_info)
    {
        if (self::shouldVerifyAddress($user_info)) {
            $address = self::getAddress($user_info);

            return self::checkAddress($address, $user_info);
        }
        
        return $user_info;
    }

    /**
     * Query the address in Smarty Streets. Return an array if valid, otherwise empty.
     * 
     * @param  array $user_info
     * @return mixed
     */
    protected static function getAddress(array $user_info)
    {
        $address = self::getSmartyStreetsArray($user_info);

        return SmartyStreets::addressQuickVerify($address);
    }

    /**
     * Check if the address is valid and unique. If positive, return the address formated.
     * 
     * @param  array  $smarty_address
     * @param  array  $user_info
     * @return mixed
     */
    protected static function checkAddress(array $smarty_address, array $user_info)
    {
        if (empty($smarty_address)) {
            return self::$INVALID_STATUS;
        } elseif (self::checkDuplicatedAddress($smarty_address, $user_info['email'])) {
            return self::$DUPLICATED_STATUS;
        } else {
            return self::getFormatedAddress($smarty_address, $user_info);
        }
    }

    /**
     * Check if another user has the same address.
     * 
     * @param  array   $smarty_address
     * @param  string  $email
     * @return boolean
     */
    protected static function checkDuplicatedAddress(array $smarty_address, $email)
    {
        return User::where('loc_lat', $smarty_address['metadata']['latitude'])
            ->where('loc_lng', $smarty_address['metadata']['longitude'])
            ->where('email', '!=', $email)->first();
    }

    /**
     * Check if the required fields for the Smarty Address query are provided.
     * 
     * @param  array   $user_info
     * @return boolean
     */
    protected static function shouldVerifyAddress(array $user_info)
    {
        return isset($user_info['firm_address_1']) && 
               isset($user_info['firm_city']) && 
               isset($user_info['firm_state']);
    }

    /**
     * Return the user address information as an array to be used in the Smarty Streets query.
     * The zip fields isn't required, so can we empty it if the user don't provide one.
     * 
     * @param  array $user_info
     * @return array
     */
    protected static function getSmartyStreetsArray(array $user_info)
    {
        $user_info['firm_zip'] = (isset($user_info['firm_zip'])) ?: '';

        return [
            'street' => $user_info['firm_address_1'],
            'city'   => $user_info['firm_city'],
            'state'  => $user_info['firm_state'],
            'zip'    => $user_info['firm_zip'],
        ];
    }

    /**
     * Put the given Smarty Address values in the user information array.
     * 
     * @param  array $smarty_address
     * @param  array $user_info     
     * @return array               
     */
    protected static function getFormatedAddress(array $smarty_address, array $user_info)
    {
        $user_info['firm_address_1'] = $smarty_address['components']['primary_number'] . ' ' . $smarty_address['components']['street_name'];
        $user_info['firm_city']      = $smarty_address['components']['city_name'];
        $user_info['firm_state']     = $smarty_address['components']['state_abbreviation'];
        $user_info['firm_zip']       = $smarty_address['components']['zipcode'];
        $user_info['loc_lat']        = $smarty_address['metadata']['latitude'];
        $user_info['loc_lng']        = $smarty_address['metadata']['longitude'];

        return $user_info;
    }
}