<?php

/*
|--------------------------------------------------------------------------
| EmailTestHelper.php
|--------------------------------------------------------------------------
| 
| This class helps email integration tests when using the Laravel Log 
| Driver by checking if the email was sent with the provided content.
| 
*/

class EmailTestHelper
{
    /**
     * True if should clean the logs after finishing tests.
     * Tip: set this value to 'false' when sending multiple emails at once, 
     * otherwise, the log file will be cleaned and the test will 
     * fail, even through your email was sent.
     * @var boolean
     */
    protected $clean = true;

    /**
     * True if the data should be in the logs, false otherwise.
     * @var boolean
     */
    protected $assert = true;

    /**
     * Log file path.
     * @var string|empty
     */
    protected $path;

    /**
     * A log file path may be provided, otherwise, we use the default Laravel log file.
     * 
     * @param string $path
     */
    public function __construct($path = '')
    {

        $this->path = $path;   
    }

    /**
     * Check if the values in $data are present on the Log File.
     * 
     * @param  array   $data  Values to be checked e.g. ['name' => 'John', 'email' => ...]
     * @param  boolean $clean If should clean the logs after finishing the test.
     * @return mixed          Raises a TestCase assertion
     */
    public function isInLogs(array $data, $clean = true)
    {
        $this->clean = $clean;

        $this->assert = true;

        $logs = $this->getLogFile();

        return $this->assert($data, $logs);
    }

    /**
     * Check if the values in $data are NOT present on the Log File.
     * 
     * @param  array   $data  Values to be checked e.g. ['name' => 'John', 'email' => ...]
     * @param  boolean $clean If should clean the logs after finishing the test.
     * @return mixed          Raises a TestCase assertion
     */
    public function isNotInLogs(array $data, $clean = true)
    {
        $this->clean = $clean;

        $this->assert = false;

        $logs = $this->getLogFile();

        return $this->assert($data, $logs);
    }

    /**
     * Return the log file contents.
     * 
     * @return string
     */
    protected function getLogFile()
    {
        $path = $this->getLogPath();

        return file_get_contents($path);
    }

    /**
     * Get the provided log path or a default, if none.
     * 
     * @return string
     */
    protected function getLogPath()
    {
        return $this->path ?: storage_path() . '/logs/laravel.log';
    }

    /**
     * Clean the log file.
     * 
     * @return null
     */
    public function cleanLogFile()
    {
        $path = $this->getLogPath();

        file_put_contents($path, '');
    }

    /**
     * Check if the given word is in the given string.
     * 
     * @param  string  $word   
     * @param  string  $string 
     * @return boolean 
     */
    protected function isInString($word, $string)
    {
        return preg_match("~\b$word\b~", $string) > 0;
    }

    /**
     * Make an assertion that the given array is or is not in the given log content. 
     * For example, given the array ['name' => 'Test', 'email' => 'test@test.com'], 
     * and that $this->assert is true, it will fail any of the strings 
     * 'Test' or 'test@test.com' are not in the logs content.
     * 
     * @param  array  $data Data to be tested
     * @param  string $logs Log file content
     * @return mixed        Raises a TestCase assertion
     */
    protected function assert(array $data, $logs)
    {
        foreach ($data as $key => $value) {
            // Check if the given word is in the logs.
            $assertion = $this->isInString($value, $logs);

            // Simply invert the assertion result if $this->assert is false.
            $assertion = $this->assert ? $assertion : !$assertion;

            // The key must not be blacklisted.
            if (!$this->isBlacklisted($key) AND !$assertion) {
                !$this->clean ?: $this->cleanLogFile();

                $message = $this->getFailMessage($key, $value);
                
                TestCase::fail($message);
            }
        }

        // Must try to clean the logs two times, because 
        // TestCase::fail() stops the program execution.
        !$this->clean ?: $this->cleanLogFile();

        return TestCase::assertTrue(true);
    }    

    /**
     * Check if the given string is blacklisted and should be ignored when making assertions.
     * 
     * @param  string  $string  Word to be checked
     * @return boolean          True if blacklisted
     */ 
    protected function isBlacklisted($string)
    {
        return in_array($string, config('messages.blacklist'));
    }

    /**
     * Get a custom fail message.
     * 
     * @param  Field name   $key
     * @param  Value tested $value
     * @return string
     */
    protected function getFailMessage($key, $value)
    {
        $option = !$this->assert ? ' not' : '';

        return "The $key field with value of $value is$option in the Log File.";
    }
}