<?php

namespace App\Http\Controllers;

use Auth;
use JWTAuth;
use JWTFactory;
use Validator;
use App\Client;
use App\Product;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helpers\ApiResponseHelper;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->only('postLogout', 'getProduct', 'postProduct');
    }

    /**
     * Displays the login page.
     * 
     * @param  Request $request
     * @return View
     */
    public function getLogin(Request $request)
    {
        return view('auth.login');
    }

    /**
     * Puts the user in the session and redirects to the choose product page.
     * 
     * @param  Request $request
     * @return View
     */
    public function postLogin(Request $request)
    {
        $credentials = $request->only(['email', 'pword_hash']); 

        $this->validate($request, [ 
            'email' => 'email|required',
            'pword_hash' => 'required'
        ]);

        if (Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['pword_hash']])) {
            return redirect()->route('product.get');
        }
        
        return back()->withErrors(['errors' => 'Incorrect username or password.'])->withInput();
    }

    /**
     * Logs in the user if the credentials match.
     * 
     * @param  Request $request
     * @return JSON
     */
    public function loginAPI(Request $request)
    {
        $credentials = $request->only(['email', 'pword_hash']); 

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'pword_hash' => 'required',
        ]);

        if ($validator->fails()) {
            return ApiResponseHelper::make(2, $validator->errors()->all(), 422);
        }

        try {
            if (!Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['pword_hash']])) {
                return ApiResponseHelper::make(3, 'Unauthorized', 401);
            } else {
                $client = Client::where('email', $credentials['email'])->first();
                $payload = JWTFactory::sub($client->clients_id)->make();
                $token = JWTAuth::encode($payload);

                return ApiResponseHelper::make(1, 'Login successfully.', 200, ['token' => $token->get()]);
            }
        } catch (JWTException $e) {
            return ApiResponseHelper::make(9, 'Could not create token', 500);
        }
    }

    /**
     * Logs out the user and removes his product from the session.
     * 
     * @param  Request $request
     * @return View
     */
    public function postLogout(Request $request)
    {
        Auth::logout();
        $request->session()->forget('product');
        
        return redirect()->route('login.get');
    }

    /**
     * Displays the choose product page.
     * 
     * @param  Request $request
     * @return View
     */
    public function getProduct(Request $request)
    {
        $products = Product::where('clients_id', Auth::user()->clients_id)->get();

        return view('auth.products', compact('products'));
    }

    /**
     * Add the choosen product to the session.
     * 
     * @param  Request $request
     * @return View
     */
    public function postProduct(Request $request)
    {
        $product = Product::where('clients_id', Auth::user()->clients_id)
            ->where('product_name', $request->input('product_name'))
            ->first();
        
        if ($product) {
            $request->session()->put('product', $product->products_id);
            
            return redirect()->to('/dashboard');
        }

        return back()->withErrors(['errors' => 'A product is required.']);
    }
}
